var spawn = require('child_process').spawn; 
var http = require('http');

var gatt = spawn('gatttool', ['-I']);
var curentValueOnMultimeter = ''; 



process.on('SIGINT', function(){
	disconnectMultimeter(); 
	process.exit(0); 
});

//******************************************
//******************************************
//*************SERVER***********************
//******************************************
//******************************************
http.createServer(function(req, res) {
	
	if (req.url == '/getIndication'){
		res.end(curentValueOnMultimeter);
	} else {
			res.statusCode = 404; 
			res.end('Bad gateway');
		}	
}).listen(3000); 


//******************************************
//******************************************
//*************OWON*************************
//******************************************
//******************************************

function disconnectMultimeter(){
	gatt.stdin.end('disconnect\n');
}

//***************************connect to OWON
gatt.stdin.write('connect 88:4A:EA:65:A3:4D\n'); 

gatt.stderr.on('data', function(chunk) {
    console.log("Error: ", chunk);
}); 

gatt.on('error', function(err) {
    console.log(err); 
});

//***************************************************processing a data from the multimeter
gatt.stdout.on('data', function(chunk){

	const strNotification = 'Notification handle = 0x002e value: '; 
	var strAnswer = chunk.toString(); 
	var startIndex = strAnswer.indexOf(strNotification);
	
	if (startIndex != -1) {
		var strValue = strAnswer.substring(startIndex + strNotification.length - 1);
		
		var bytes = strValue.split(' ');
		if(bytes.length == 17){
			curentValueOnMultimeter = parseBytesToStr(bytes);
			console.log(curentValueOnMultimeter); 
		} 
	} 
});



//********************************
//*****PARSER*********************
//*****OF*************************
//*****PACKETS********************
//********************************
function parseBytesToStr(bytes){
	var _str = ''; 

	//get sign of value 
	if (bytes[1] == '2b'){
		_str += '+'}
	else { _str += '-'}

	//get point positon
	var _pointPosition = getPointPosition(bytes[7]) + 2; 

	//get value 
	for (var i = 2; i < 6; i ++) 
	{ 
		if(_pointPosition == i && _pointPosition >2)
		{_str += '.';} 
		_str += ASCIIToNumber(bytes[i]);
	}  

	//get units of value 
	_str += ' ' + getUnits(bytes[10], bytes[11]);
	
return _str;
}


//********************************
//*****HELPFULL*******************
//*****FUNCTIONS******************
//*****FOR************************
//*****THE************************
//*****PACKET*********************
//*****PARSER*********************
//********************************
function getUnits(byte1, byte2){
	
	var units = ''; 
	
	//mV
	if (byte1 == '40' && byte2 == 80) 
		{ units = 'mV';}
	//V
	else if (byte1 == '00' && byte2 == '80')
		{units = 'V';}
	//Ohm
	else if (byte1 == '00' && byte2 == '20')
		{units = 'Ohm';}
	//kOhm
	else if (byte1 == '20' && byte2 == '20')
		{units = 'kOhm';}
	//MOhm
	else if (byte1 == '10' && byte2 == '20')
		{units = 'V';}
	//A
	else if (byte1 == '00' && byte2 == '40')
		{units = 'A';}
	//mA
	else if (byte1 == '40' && byte2 == '40')
		{units = 'mA';}
	//uA
	else if (byte1 == '80' && byte2 == '40')
		{units = 'uA';}
	//C
	else if (byte1 == '00' && byte2 == '02')
		{units = 'C';}
	//F
	else if (byte1 == '00' && byte2 == '01')
		{units = 'F';}
	//Hz
	else if (byte1 == '00' && byte2 == '08')
		{units = 'Hz';}
	//hFE
	else if (byte1 == '00' && byte2 == '10')
		{units = 'hFE';}

	//hz ego chto
	else {units = 'unknown';}

return units;
}

function ASCIIToNumber(charCode){
	var code = parseInt(charCode); 
	return (code - 30).toString();
}

function getPointPosition(byte){ 
	//31 after 1 number 
	//32 after 2 
	//34 after 3 
	var position = 0 ; 
	if (byte == '31')
	{position = 1;}
	else if (byte == '32')
	{position = 2; }
	else if (byte == '34')
	{ position = 3; }

	return position; 	
}